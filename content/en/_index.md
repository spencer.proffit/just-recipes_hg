---
title: "Just Recipes"
description: "A site for Just Recipes, no filler"
featured_image: '/bread.jpg'
---

This website is for those who love to cook, and also love to read, but want to cook right now and read all the filler stories and background.
