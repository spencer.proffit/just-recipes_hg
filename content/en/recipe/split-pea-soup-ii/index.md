---
title: "Split Pea Soup II"
author: Yorkraft, Inc.
source: Pennsylvania Dutch Cooking (1960-01-01)
categories: [Soup]
measure: avoirdupois
ingredients: [split pea, carrot, water, ham, celery, salt, onion, pepper]
equipment: [measuring cups, measuring spoons, stove, pot, knife, sieve]
---
<!--more-->
Ingredients

* 1 pound split peas
* 2 carrots, sliced
* 3 quart water
* 1 ham, bone
* 1 stalk celery, chopped
* salt
* 1 large onion, chopped
* pepper

Directions

1. Wash peas, add cold water, vegetables and ham bone and simmer for three hours or until mixture is thick.
1. Remove ham bone, force peas through coarse sieve and season to taste.
1. Dilute with milk.
1. Serve with toasted croutons.
