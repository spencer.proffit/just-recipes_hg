---
title: "Creamed Potatoes"
author: Louise Bennett Weaver, Helen Cowles LeCron
source: A Thousand Ways To Please a Husband (1917-01-01)
categories: [Side]
measure: avoirdupois
ingredients: [potato, salt, parsley, paprika, pimento, white sauce]
equipment: [measuring cups, measuring spoons, stove]
---
## Related Recipes: White Sauce

## Yield: 4 servings
<!--more-->

## Ingredients

* 2 cup cold diced potatoes
* 1/2 teaspoon salt
* 1 tablespoon chopped parsley
* 1/8 teaspoon paprika
* 1 tablespoon chopped pimento
* 1 cup vegetable white sauce

## Directions

1. Add the potatoes, sprinkled with salt and pepper, to vegetable white sauce.
1. Add pimento and parsley.
1. Cook three minutes, stirring constantly.
