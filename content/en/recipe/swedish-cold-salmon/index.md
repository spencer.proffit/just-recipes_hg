---
title: "Swedish Cold Salmon"
author: "Mrs. A. H. Lofgren Contributor: Point Adams Packing Co"
categories: [Seafood]
ingredients: [salmon, sugar, spice, lemon juice, salt, egg yolk, butter, mustard, flour, salad dressing, milk, whipped cream]
equipment: [measuring cups, measuring spoons, knife, stove, pot]
---
<!--more-->
Ingredients
* 6 - 8 pound whole salmon
* 1 tablespoon sugar
* 1 tablespoon mixed pickling spices
* 1/3 cup lemon juice
* 1/3 cup salt
* 3 egg yolks
* 6 tablespoon butter
* 1 teaspoon dry mustard
* 3 tablespoon flour
* 3/4 cup salad dressing, (not mayonnaise)
* 1 cup milk
* salt
* 1 cup whipped cream

Directions

1. Scale fish and clean thoroughly.
1. Rinse well.
1. Place a cheesecloth in a large kettle and place the fish in this.
1. The cloth should be large enough to be knotted over the fish so the fish can be lifted out with the cloth.
1. Add water, salt, and the pickling spices.
1. Cover and simmer for about 40 minutes or until the fish is just tender.
1. Remove cover and let fish stand in broth overnight in refrigerator.
1. Lift out fish carefully.
1. Remove skin and backbone, keeping the fish as whole as possible.
1. Put the two sides of fish together on a platter.
1. Make a sauce by melting the butter.
1. Add flour, blend, add milk.
1. Cook and stir to thick sauce.
1. Salt to taste and add sugar.
1. Mix lemon juice with egg yolks and stir in.
1. Add mustard and chill.
1. When cold, fold in salad dressing and whipped cream spread over salmon that has been chilled.
1. Garnish.
