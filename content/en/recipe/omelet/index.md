---
title: "Omelet"
author: Louise Bennett Weaver, Helen Cowles LeCron
source: A Thousand Ways To Please a Husband (1917-01-01)
categories: [Breakfast]
measure: avoirdupois
ingredients: [egg, pepper, water, butter, salt, parsley]
equipment: [measuring spoons, knife, egg beater, stove]
---
## Yield: 4 servings
<!--more-->
## Ingredients

* 4 eggs
* 1/8 teaspoon pepper
* 4 tablespoon hot water
* 1 tablespoon butter
* 1/2 teaspoon salt
* a little parsley

## Directions

1. Beat the yolks until thick and lemon colored.
1. Add hot water (one tablespoonful to an egg), salt and pepper.
1. Beat the whites till stiff and dry.
1. Cut and fold into the first mixture.
1. Heat the omelet pan, add the butter, turn the pan so that the melted butter covers the sides and bottom of the pan.
1. Turn in the mixture, spread evenly, turn down the fire and allow the omelet to cook slowly.
1. Turn the pan so that the omelet will brown evenly.
1. When well puffed and delicately browned underneath, place the pan on the center shelf in a moderate oven to finish cooking the top of the omelet.
1. Crease across center with knife and fold over very carefully.
1. Allow to remain a moment in pan.
1. Turn gently with a spatula onto a hot platter.
1. Garnish with parsley.
1. An omelet is sufficiently cooked when it is firm to the touch when pressed by the finger.
