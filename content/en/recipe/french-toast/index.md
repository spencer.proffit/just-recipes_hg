---
title: "French Toast"
featured_image: '/bread.jpg'
author: Evaporated Milk Association
source: Quantity Recipes for Quality Foods (1939-01-01)
categories: [Bread, Breakfast]
measure: avoirdupois
ingredients: [egg, salt, sugar, evaporated milk, water, bread]
equipment: [measuring spoons, measuring cups, egg beater, griddle]
---
Yield: 50 servings
<!--more-->
Ingredients

* 24 eggs
* 2 tablespoon salt
* 1 1/2 cup sugar
* 2 1/4 quart evaporated milk
* 2 1/4 quart water
* 100 slices bread

Directions

1. Beat eggs, add salt, sugar, milk and water.
1. Soak both sides of bread in the mixture.
1. Lift carefully to hot, well greased griddle.
1. Brown on both sides.
1. Sprinkle lightly with sugar and serve at once, plain, or with syrup or jelly.
1. French toast is delicious if butter is used for frying.
