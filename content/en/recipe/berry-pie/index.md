---
title: "Berry Pie"
author: Louise Bennett Weaver, Helen Cowles LeCron
categories: [Dessert]
ingredients: [berry, flour, salt, sugar, lemon juice]
equipment: [measuring cups, measuring spoons, oven, rolling pin]
source: A Thousand Ways To Please a Husband (1917-01-01)
measure: avoirdupois
---
## Yield: 4 servings
<!--more-->

## Ingredients

* 1 1/2 cup berries, (black or blue)
* 2 tablespoon flour
* 1/8 teaspoon salt
* 1/2 cup sugar
* 1 tablespoon lemon juice

Directions

1. Wash the fruit, mix with the sugar, flour, salt and lemon juice.
1. Line a deep pie tin with a plain pie paste and sprinkle one tablespoon sugar over bottom crust.
1. Add the berry mixture.
1. Wet the lower crust slightly.
1. Roll out the upper crust and make slits in the middle to allow the steam to escape.
1. Place on the lower crust, pinching the edges together.
1. Bake in a moderately hot oven forty minutes.
